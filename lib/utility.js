const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const rimraf = require('rimraf')
const replace = require('replace')

module.exports = {
  createComponents (components, dist) {
    components.forEach(component => {
      const obj = {
        html: `${component}.html`,
        scss: `${component}.scss`,
        js: `${component}.js`
      }

      const dirname = path.normalize(`${dist}/src/components/${component}`)
      const include = `@import '../components/${component}/${obj.scss}';`

      if (!fs.existsSync(dirname)) {
        fs.mkdirSync(dirname)

        for (let prop in obj) {
          if (obj.hasOwnProperty(prop)) {
            fs.writeFile(`${dirname}/${obj[prop]}`, '', err => {
              if (err) throw err
            })
          }
        }

        fs.appendFile(`${dist}/src/sass/includeComponents-lock.scss`, `${include}\n`, 'utf8', err => {
          if (err) throw err
        })
      }
    })
    console.log(chalk.green(`The components - ${components} was successfully created!`))
  },

  removeComponents: function (components, dist) {
    components.forEach(component => {
      let rule = new RegExp(`@import '../components/${component}/${component}.scss';\n`, 'g')
      const path = `${dist}/src/sass/includeComponents-lock.scss`
      rimraf(`${dist}/src/components/${component}`, () => { console.log(`${component} remove`) })
      replace({
        regex: rule,
        replacement: '',
        paths: [path],
        recursive: true,
        silent: true
      })
    })
  },

  isEmpty (arg) {
    return arg.length >= 1
  }
}
